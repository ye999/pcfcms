<?php
/***********************************************************
 * 公共模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\Model;
class Common extends Model
{
    /**
     * 返回layui的table所需要的格式
     * @author sin
     * @param $post
     * @return mixed
     */
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->tableWhere($post);
        $list = $this->field($tableWhere['field'])->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
        $data = $this->tableFormat($list->items());
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list->total(),'data' => $data];
        return $result;
    }

    /**
     * 根据输入的查询条件，返回所需要的where
     * @author sin
     * @param $post
     * @return mixed
     */
    protected function tableWhere($post)
    {
        $result['where'] = [];
        $result['field'] = "*";
        $result['order'] = [];
        return $result;
    }

    /**
     * 根据查询结果，格式化数据
     * @author sin
     * @param $list
     * @return mixed
     */
    protected function tableFormat($list)
    {
        return $list;
    }


    //错误提醒页面
    public function errorNotice($msg = '操作失败',$backUrl = '',$info = '',$wait = 3)
    {
        $type = false;
        $this->assign(compact('msg','backUrl','info','wait','type'));
        exit($this->fetch('public/error'));
    }
    
    //成功提醒页面
    public function successNotice($msg = '操作成功',$backUrl = '',$info = '',$wait = 3)
    {
        $type = true;
        $this->assign(compact('msg','backUrl','info','wait','type'));
        return $this->fetch('public/success');
    }
   
}