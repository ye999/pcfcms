<?php
/***********************************************************
 * 编辑器
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\system;
use app\admin\controller\Base;
use think\facade\Request;
use think\facade\Db;
use think\facade\Session;
use think\facade\Filesystem;
use think\facade\Validate;
use think\File;

class Ueditor extends Base
{
    private $sub_name = array('date', 'Ymd');
    private $fileExt = "jpg,png,gif,jpeg,bmp,ico";
    private $nowFileName = '';

    public function initialize(){
        parent::initialize();
        date_default_timezone_set("Asia/Shanghai");
        $this->nowFileName = input('nowfilename', '');
        if (empty($this->nowFileName)) {
            $this->nowFileName = md5(time().uniqid(mt_rand(), TRUE));
        }
        error_reporting(E_ERROR | E_WARNING);
        header("Content-Type: text/html; charset=utf-8");
        $image_type = tpCache('basic.image_type');
        $this->fileExt = !empty($image_type) ? str_replace('|', ',', $image_type) : $this->fileExt;
    }

    public function index()
    {
        $CONFIG2 = json_decode(preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents(root_path()."public/common/plugins/Ueditor/php/config.json")), true);
        $action = $_GET['action'];
        switch ($action) {
            case 'config':
                $result =  json_encode($CONFIG2);
                break;
            /* 上传图片 */
            case 'uploadimage':
                $fieldName = $CONFIG2['imageFieldName'];
                $result = $this->upFile($fieldName);
                break;
            /* 上传涂鸦 */
            case 'uploadscrawl':
                $config = array(
                    "pathFormat" => $CONFIG2['scrawlPathFormat'],
                    "maxSize" => $CONFIG2['scrawlMaxSize'],
                    "allowFiles" => $CONFIG2['scrawlAllowFiles'],
                    "oriName" => "scrawl.png"
                );
                $fieldName = $CONFIG2['scrawlFieldName'];
                $base64 = "base64";
                $result = $this->upBase64($config,$fieldName);
                break;
            /* 上传视频 */
            case 'uploadvideo':
                $fieldName = $CONFIG2['videoFieldName'];
                $result = $this->upFile($fieldName);
                break;
            /* 上传文件 */
            case 'uploadfile':
                $fieldName = $CONFIG2['fileFieldName'];
                $result = $this->upFile($fieldName);
                break;
            /* 列出图片 */
            case 'listimage':
                $allowFiles = $CONFIG2['imageManagerAllowFiles'];
                $listSize = $CONFIG2['imageManagerListSize'];
                $path = $CONFIG2['imageManagerListPath'];
                $get =$_GET;
                $result =$this->fileList($allowFiles,$listSize,$get);
                break;
            /* 列出文件 */
            case 'listfile':
                $allowFiles = $CONFIG2['fileManagerAllowFiles'];
                $listSize = $CONFIG2['fileManagerListSize'];
                $path = $CONFIG2['fileManagerListPath'];
                $get = $_GET;
                $result = $this->fileList($allowFiles,$listSize,$get);
                break;
            /* 抓取远程文件 */
            case 'catchimage':
                $config = array(
                    "pathFormat" => $CONFIG2['catcherPathFormat'],
                    "maxSize" => $CONFIG2['catcherMaxSize'],
                    "allowFiles" => $CONFIG2['catcherAllowFiles'],
                    "oriName" => "remote.png"
                );
                $fieldName = $CONFIG2['catcherFieldName'];
                /* 抓取远程图片 */
                $list = array();
                isset($_POST[$fieldName]) ? $source = $_POST[$fieldName] : $source = $_GET[$fieldName];
                foreach($source as $imgUrl){
                    $info = json_decode($this->saveRemote($config,$imgUrl),true);
                    array_push($list, array(
                        "state" => $info["state"],
                        "url" => $info["url"],
                        "size" => $info["size"],
                        "title" => htmlspecialchars($info["title"]),
                        "original" => str_replace("&amp;", "&", htmlspecialchars($info["original"])),
                        "source" => str_replace("&amp;", "&", htmlspecialchars($imgUrl))
                    ));
                }
                $result = json_encode(array(
                    'state' => !empty($list) ? 'SUCCESS':'ERROR',
                    'list' => $list
                ));
                break;
            default:
                $result = json_encode(array(
                    'state' => '请求地址出错'
                ));
                break;
        }
        // 输出结果
        if(isset($_GET["callback"])){
            if(preg_match("/^[\w_]+$/", $_GET["callback"])){
                echo htmlspecialchars($_GET["callback"]).'('.$result.')';
            }else{
                echo json_encode(array('state' => 'callback参数不合法'));
            }
        }else{
            echo $result;
        }
    }

    // 图片上传 
    public function imageUp()
    {
        //文件上传到哪里       
        if (!Request::isAjax()) {
            $return_data['state'] = '非法上传';
            respose($return_data,'json');
        }
        $image_upload_limit_size = intval(tpCache('basic.file_size') * 1024 * 1024);
        // 获取表单上传文件
        $file = request()->file('file');
        if(empty($file)){
            $file = request()->file('upfile');
        }
        $tptype = pathinfo($file->getoriginalName(), PATHINFO_EXTENSION);
        $tptype = strtolower($tptype);
        $newfileExt = explode(",",$this->fileExt);
        //ico图片文件不进行验证
        if ($tptype != 'ico') {
            if ($image_upload_limit_size <= $file->getSize()) {
                $state = "ERROR：上传文件过大";
                $return_data['state'] = $state;
                return $return_data;
            }
            if (!in_array($tptype, $newfileExt)) {
                $state = "ERROR：上传文件后缀名必须为".$this->fileExt;
                $return_data['state'] = $state;
                return $return_data; 
            }
        }
        //验证图片一句话木马
        $imgstr = @file_get_contents($file->getoriginalName());
        if (false !== $imgstr && preg_match('#<\?php#i', $imgstr)) {
            $state = "ERROR：上传图片不合格";
            $return_data['state'] = $state;
            return $return_data;
        }
        if (!empty($file)) {
            $ossConfig = tpCache('oss');
            if ($ossConfig['oss_switch'] > 0) {
                $uptype = "Aliyun";
                //这是上传到第三方图片库
            } else {
                $uptype = "local";
                $fileName = Filesystem::disk('local')->putFile('allimg', $file);
                if ($fileName) {
                    $state = "SUCCESS";
                } else {
                    $state = "ERROR" . '上传图片失败！';
                }
                $fileName = str_replace("\/", "/", $fileName);
                $fileName = str_replace("\\", "/", $fileName);   
            }
        } 
        if($state == 'SUCCESS' && pathinfo($file->getoriginalName(), PATHINFO_EXTENSION) != 'ico'){
            // 添加水印
            $imgresource = "./uploads/".$fileName;
            $image = \think\Image::open($imgresource);
            $water = tpCache('water');
            $return_data['mark_type'] = $water['mark_type'];
            if($water['is_mark']==1 && $image->width()>$water['mark_width'] && $image->height()>$water['mark_height']){
                if($water['mark_type'] == 'text'){
                    $ttf = public_path().'public/common/font/hgzb.ttf';
                    if (file_exists($ttf)) {
                        $size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
                        $color = $water['mark_txt_color'] ? $water['mark_txt_color'] : '#000000';
                        if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
                            $color = '#000000';
                        }
                        $transparency = intval((100 - $water['mark_degree']) * (127/100));
                        $color.= dechex($transparency);
                        $image->open($imgresource)->text($water['mark_txt'], $ttf, $size, $color, $water['mark_sel'])->save($imgresource);
                        $return_data['mark_txt'] = $water['mark_txt'];
                    }
                }else{
                    $water['mark_img'] = preg_replace('#^(/[/\w]+)?(/public/uploads/|/uploads/)#i', '$2', $water['mark_img']); // 支持子目录
                    $waterPath = public_path()."public".$water['mark_img'];
                    $waterPath = str_replace("\/", "/", $waterPath);
                    if (eyPreventShell($waterPath) && file_exists($waterPath)) {
                        $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                        $waterTempPath = dirname($waterPath).'/temp_'.basename($waterPath);
                        $image->open($waterPath)->save($waterTempPath, null, $quality);
                        $image->open($imgresource)->water($waterTempPath, $water['mark_sel'], $water['mark_degree'])->save($imgresource);
                        @unlink($waterTempPath);
                    }
                }
            }

            $iData['pid']    = md5('/uploads/'.$fileName);
            $iData['type']  = $uptype;
            $iData['name']  = $file->getoriginalName();
            $iData['url']   = '/uploads/'.$fileName;
            $iData['ctime'] = time();
            $iData['path']  = '/uploads/'.$fileName;
            $pcfcmsuppic = Db::name('images')->save($iData);
            if($pcfcmsuppic){
                $state = "SUCCESS"; 
            }else{
                $state = "ERROR" . '上传图片失败！';
            }
            
        }
        $return_data['url'] = '/uploads/'.$fileName; // 支持子目录 
        $return_data['state'] = $state;
        return $return_data;
    }

    // 在编辑器上传 
    private function upFile($fieldName)
    {
        $file = request()->file($fieldName);
        if(empty($file)){
            return json_encode(['state' =>'ERROR，请上传文件']);
        }
        $image_upload_limit_size = intval(tpCache('basic.file_size') * 1024 * 1024);
        $fileExt = '';
        $image_type = tpCache('basic.image_type');
        !empty($image_type) && $fileExt .= '|'.$image_type;
        $file_type = tpCache('basic.file_type');
        !empty($file_type) && $fileExt .= '|'.$file_type;
        $media_type = tpCache('basic.media_type');
        !empty($media_type) && $fileExt .= '|'.$media_type;
        $fileExt = !empty($fileExt) ? str_replace('||', '|', $fileExt) : "jpg,png,gif,jpeg,bmp,ico";
        $fileExt = str_replace('|', ',', trim($fileExt, '|'));
        $tptype = pathinfo($file->getoriginalName(), PATHINFO_EXTENSION);
        $tptype = strtolower($tptype);
        $newfileExt = explode(",",$fileExt);
        if ($image_upload_limit_size <= $file->getSize()) {
            $state = "ERROR：上传文件过大";
            return json_encode(['state' =>$state]);
        }
        if (!in_array($tptype, $newfileExt)) {
            $state = "ERROR：上传文件后缀名必须为".$fileExt;
            return json_encode(['state' =>$state]);
        }
        $ossConfig = tpCache('oss');
        if ($ossConfig['oss_switch'] >0) {
            $uptype = "Aliyun";
        } else {
            $uptype = "local";
            $fileName = Filesystem::disk('local')->putFile('ueditor', $file);
            if ($fileName) {
                $state = "SUCCESS";
            } else {
                $state = "ERROR" . '上传图片失败！';
            }
            $fileName = str_replace("\/", "/", $fileName);
            $fileName = str_replace("\\", "/", $fileName);   
        }
        if($state == 'SUCCESS'){
            $data = array(
                'state' => 'SUCCESS',
                'url' => '/uploads/'.$fileName,
                'title' => $file->getoriginalName(),
                'original' => $file->getoriginalName(),
                'type' => '.' . $tptype,
                'size' => $file->getSize(),
            );
            //图片加水印
            if($data['state'] == 'SUCCESS'){
                $file_type = $tptype;
                if ($file_type && 'ico' != $file_type) {
                    $imgresource = "./uploads/".$fileName;
                    $image = \think\Image::open($imgresource);
                    $water = tpCache('water');
                    $return_data['mark_type'] = $water['mark_type'];
                    if($water['is_mark']==1 && $image->width()>$water['mark_width'] && $image->height()>$water['mark_height']){
                        if($water['mark_type'] == 'text'){
                            $ttf = public_path().'public/common/font/hgzb.ttf';
                            if (file_exists($ttf)) {
                                $size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
                                $color = $water['mark_txt_color'] ? $water['mark_txt_color'] : '#000000';
                                if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
                                    $color = '#000000';
                                }
                                $transparency = intval((100 - $water['mark_degree']) * (127/100));
                                $color.= dechex($transparency);
                                $image->open($imgresource)->text($water['mark_txt'], $ttf, $size, $color, $water['mark_sel'])->save($imgresource);
                                $return_data['mark_txt'] = $water['mark_txt'];
                            }
                        }else{
                            $water['mark_img'] = preg_replace('#^(/[/\w]+)?(/public/uploads/|/uploads/)#i', '$2', $water['mark_img']); // 支持子目录
                            $waterPath = public_path()."public".$water['mark_img'];
                            $waterPath = str_replace("\/", "/", $waterPath);
                            if (eyPreventShell($waterPath) && file_exists($waterPath)) {
                                $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                                $waterTempPath = dirname($waterPath).'/temp_'.basename($waterPath);
                                $image->open($waterPath)->save($waterTempPath, null, $quality);
                                $image->open($imgresource)->water($waterTempPath, $water['mark_sel'], $water['mark_degree'])->save($imgresource);
                                @unlink($waterTempPath);
                            }
                        }
                    }

                    $iData['pid']    = md5('/uploads/'.$fileName);
                    $iData['type']  = $uptype;
                    $iData['name']  = $file->getoriginalName();
                    $iData['url']   = '/uploads/'.$fileName;
                    $iData['ctime'] = time();
                    $iData['path']  = '/uploads/'.$fileName;
                    $pcfcmsuppic = Db::name('images')->save($iData);
                    if($pcfcmsuppic){
                        $state = "SUCCESS"; 
                    }else{
                        $state = "ERROR" . '上传图片失败！';
                    }


                }
            }
        }else{
            $data = array('state' => 'ERROR');
        }
        return json_encode($data);
    }

    // 列出图片 
    private function fileList($allowFiles,$listSize,$get)
    {
        $dirname = root_path().'public/uploads/';
        $allowFiles = substr(str_replace(".","|",join("",$allowFiles)),1);
        /* 获取参数 */
        $size = isset($get['size']) ? htmlspecialchars($get['size']) : $listSize;
        $start = isset($get['start']) ? htmlspecialchars($get['start']) : 0;
        $end = $start + $size;
        /* 获取文件列表 */
        $path = $dirname;
        $files = $this->getFiles($path,$allowFiles);
        if(empty($files)){
            return json_encode(array(
                "state" => "no match file",
                "list" => array(),
                "start" => $start,
                "total" => count($files)
            ));
        }
        /* 获取指定范围的列表 */
        $len = count($files);
        for($i = min($end, $len) - 1, $list = array(); $i < $len && $i >= 0 && $i >= $start; $i--){
            $list[] = $files[$i];
        }
        /* 返回数据 */
        $result = json_encode(array(
            "state" => "SUCCESS",
            "list" => $list,
            "start" => $start,
            "total" => count($files)
        ));
        return $result;
    }

    // 遍历获取目录下的指定类型的文件 
    // 小潘 by 2020.03.05
    private function getFiles($path,$allowFiles,&$files = array())
    {
        if(!is_dir($path)) return null;
        if(substr($path,strlen($path)-1) != '/') $path .= '/';
        $handle = opendir($path);
        while(false !== ($file = readdir($handle))){
            if($file != '.' && $file != '..'){
                $path2 = $path.$file;
                if(is_dir($path2)){
                    $this->getFiles($path2,$allowFiles,$files);
                }else{
                    $pcfpath = str_replace(root_path().'public',"/",$path2);;
                    if(preg_match("/\.(".$allowFiles.")$/i",$file)){
                        $files[] = array(
                            'url' => substr($pcfpath,1),
                            'mtime' => filemtime($path2)
                        );
                    }
                }
            }
        }       
        return $files;
    }

    // 远程图片本地化 
    // 小潘 by 2020.03.23
    public function ajax_remote_to_local()
    {
        $result = ['status' => false,'msg' => '失败','body' => ''];
        if (Request::isAjax()) {
            $body = input('post.body/s', '', null);
            $body = remote_to_local($body);
            $result['status'] = true;
            $result['msg']    = '本地化成功！';
            $result['body']   = $body;
            return $result;
        }
        $result['status'] = false;
        $result['msg']    = '本地化失败！';
        return $result; 
    }

    // 清除非站内链接 
    // 小潘 by 2020.03.23
    public function ajax_replace_links()
    {
        if (Request::isAjax()) {
            $body = input('post.body/s', '', null);
            $body = replace_links($body);
            $result = ['status' => true, 'msg' => '清除成功','body' =>$body];
            return $result;
        }
        $result = ['status' => false, 'msg' => '清除失败！'];
        return $result;
    }

    // 抓取远程图片
    // 小潘 by 2020.03.23
    private function saveRemote($config,$fieldName)
    {
        $imgUrl = htmlspecialchars($fieldName);
        $imgUrl = str_replace("&amp;","&",$imgUrl);
        //http开头验证
        if(strpos($imgUrl,"http") !== 0){
            $data=array('state' => '链接不是http链接',);
            return json_encode($data);
        }
        //获取请求头并检测死链
        $heads = get_headers($imgUrl);
        if(!(stristr($heads[0],"200") && stristr($heads[0],"OK"))){
            $data=array('state' => '链接不可用',);
            return json_encode($data);
        }
        //格式验证(扩展名验证和Content-Type验证)
        if(preg_match("/^http(s?):\/\/mmbiz.qpic.cn\/(.*)/", $imgUrl) != 1){
            $fileType = strtolower(strrchr($imgUrl,'.'));
            if(!in_array($fileType,$config['allowFiles']) || (isset($heads['Content-Type']) && stristr($heads['Content-Type'],"image"))){
                $data=array('state' => '链接contentType不正确',);
                return json_encode($data);
            }
        }
        //打开输出缓冲区并获取远程图片
        ob_start();
        $context = stream_context_create(
            array('http' => array(
                'follow_location' => false
            ))
        );
        readfile($imgUrl,false,$context);
        $img = ob_get_contents();
        ob_end_clean();
        preg_match("/[\/]([^\/]*)[\.]?[^\.\/]*$/",$imgUrl,$m);
        $dirname = '/uploads/ueditor/'.date('Ymd/');
        $file['oriName'] = $m ? $m[1] : "";
        $file['filesize'] = strlen($img);
        $file['ext'] = strtolower(strrchr($config['oriName'],'.'));
        $file['name'] = uniqid().$file['ext'];
        $file['fullName'] = $dirname.$file['name'];
        $fullName = $file['fullName'];
        //检查文件大小是否超出限制
        if($file['filesize'] >= ($config["maxSize"])){
            $data=array('state' => '文件大小超出网站限制',);
            return json_encode($data);
        }
        //创建目录失败
        if(!file_exists($dirname) && !mkdir($dirname,0777,true)){
            $data=array('state' => '目录创建失败',);
            return json_encode($data);
        }else if(!is_writeable($dirname)){
            $data=array('state' => '目录没有写权限',);
            return json_encode($data);
        }
        //移动文件
        if(!(file_put_contents($fullName, $img) && file_exists($fullName))){ //移动失败
            $data=array('state' => '写入文件内容错误',);
            return json_encode($data);
        }else{ //移动成功
            print_water($file['fullName']);
            $data=array(
                'state' => 'SUCCESS',
                'url' => $file['fullName'], // 支持子目录
                'title' => $file['name'],
                'original' => $file['oriName'],
                'type' => $file['ext'],
                'size' => $file['filesize'],
            );
        }
        return json_encode($data);
    }

    //////////////////////////////////////////////////////////////////////
    // 处理base64编码的图片上传 例如：涂鸦图片上传
    private function upBase64($config,$fieldName)
    {
        $base64Data = $_POST[$fieldName];
        $img = base64_decode($base64Data);
        $dirname = '/uploads/ueditor/'.date('Ymd/');
        $file['filesize'] = strlen($img);
        $file['oriName'] = $config['oriName'];
        $file['ext'] = strtolower(strrchr($config['oriName'],'.'));
        $file['name'] = uniqid().$file['ext'];
        $file['fullName'] = $dirname.$file['name'];
        $fullName = $file['fullName'];
        //检查文件大小是否超出限制
        if($file['filesize'] >= ($config["maxSize"])){
            $data=array(
                'state' => '文件大小超出网站限制',
            );
            return json_encode($data);
        }
        //创建目录失败
        if(!file_exists($dirname) && !mkdir($dirname,0777,true)){
            $data=array(
                'state' => '目录创建失败',
            );
            return json_encode($data);
        }else if(!is_writeable($dirname)){
            $data=array(
                'state' => '目录没有写权限',
            );
            return json_encode($data);
        }
        //移动文件
        if(!(file_put_contents($fullName, $img) && file_exists($fullName))){ //移动失败
            $data=array(
                'state' => '写入文件内容错误',
            );
        }else{ //移动成功          
            $data=array(
                'state' => 'SUCCESS',
                'url' => substr($file['fullName'],1),
                'title' => $file['name'],
                'original' => $file['oriName'],
                'type' => $file['ext'],
                'size' => $file['filesize'],
            );
        }
        return json_encode($data);
    }
    // app文件上传
    public function appFileUp()
    {
        $image_upload_limit_size = intval(tpCache('basic.file_size') * 1024 * 1024);
        $path = '/uploads/soft/'.date('Ymd/');
        if (!file_exists($path)) {
            mkdir($path);
        }
        // 获取表单上传文件
        $file = request()->file('Filedata');
        if (empty($file)) {
            $file = request()->file('upfile');    
        }

        $result = $this->validate(
            ['file2' => $file], 
            ['file2'=>'fileSize:'.$image_upload_limit_size.'|fileExt:apk,ipa,pxl,deb'],
            ['file2.fileSize' => '上传文件过大', 'file2.fileExt' => '上传文件后缀名必须为：apk,ipa,pxl,deb']                    
        );

        if (true !== $result || empty($file)) {            
            $state = "ERROR" . $result;
        } else {

            $info = $file->rule(function ($file) {    
                return date('YmdHis_').input('Filename'); //使用自定义的文件保存规则
            })->move($path);

            if ($info) {
                $state = "SUCCESS";                         
            } else {
                $state = "ERROR" . $file->getError();
            }
            $return_data['url'] = $path.$info->getSaveName();            
        }
        $return_data['title'] = 'app文件';
        $return_data['original'] = ''; 
        $return_data['state'] = $state;
        $return_data['path'] = $path;        
        respose($return_data);
    }
    /////////////////////////////////////////////////////////////////////////////

}