<?php
/***********************************************************
 * 管理员登录
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use think\facade\Cookie;
use think\facade\Cache;
use think\captcha\facade\Captcha;
use app\BaseController;
use app\admin\logic\AjaxLogic;
class Login extends BaseController
{
    //登录验证
    public function index()
    {
        $gzpcfglobal = get_global(); // 获取配置参数
        //已经登陆 ，跳转到后台首页。
        if (Session::get('admin_id')) {
            $this->redirect(Request::baseFile()); 
        }
        $is_vertify = 1;//默认开启验证码
        $admin_login_captcha = $gzpcfglobal['admin_login'];
        if (empty($admin_login_captcha['is_on']) && $admin_login_captcha['is_on'] == 0) {
            $is_vertify = 0;//不符合开启的条件
        }
        if (Request::isPost()) {
            if (!function_exists('session_start')) {
                $result = ['code' => 1, 'msg' => '请联系空间商，开启php的session扩展！'];
                return $result;
            }
            if (!testWriteAble(ROOT_PATH.'runtime/session/')) {
                $result = ['status' => 0, 'msg' => '请仔细检查以下问题：<br/>1、磁盘空间大小是否100%；<br/>2、站点目录权限是否为755；<br/>3、站点目录的权限，禁止用root ；<br/>4、如还没解决，请点击：<a href="http://www.pcfcms.com" target="_blank">查看教程</a>'];
                return $result;
            }
            $post = input('post.');
            $condition['user_name'] = input('post.username/s');
            $condition['password'] = input('post.password/s');
            if ($is_vertify == 1) {
                $pcfcode = input('post.code/s');
                if(!captcha_check($pcfcode)){
                    $result = ['code' => 1, 'msg' => '验证码错误！'];
                    return $result;
                }
            }
            if (!empty($condition['user_name']) && !empty($condition['password'])) {
                $condition['password'] = func_encrypt($condition['password']);
                $admin_info = Db::name('admin')->where($condition)->find();
                if (is_array($admin_info) && !empty($admin_info)) {
                    if ($admin_info['status'] == 0) {
                        $result = ['code' => 1, 'msg' => '账号被禁用！'];
                        return $result;
                    }
                    $role_id = !empty($admin_info['role_id']) ? $admin_info['role_id'] : -1;
                    $auth_role_info = array();
                    $role_name = !empty($admin_info['parent_id']) ? '超级管理员' : '创始人';
                    if (intval($role_id > 0)) {
                        $auth_role_info = Db::name('auth_role')
                            ->field("a.*, a.name AS role_name")
                            ->alias('a')
                            ->where('a.id','=', $role_id)
                            ->find();
                        if (!empty($auth_role_info)) {
                            $auth_role_info['permission'] = unserialize($auth_role_info['permission']);
                            $role_name = $auth_role_info['role_name'];
                        }
                    }
                    $admin_info['auth_role_info'] = $auth_role_info;
                    $admin_info['role_name'] = $role_name;
                    $last_login_time = getTime();
                    $last_login_ip = clientIP();
                    $login_cnt = $admin_info['login_cnt'] + 1;
                    Db::name('admin')->where("admin_id = ".$admin_info['admin_id'])->save(array('last_login'=>$last_login_time, 'last_ip'=>$last_login_ip, 'login_cnt'=>$login_cnt));
                    $admin_info['last_login'] = $last_login_time;
                    $admin_info['last_ip'] = $last_login_ip;
                    // 过滤存储在session文件的敏感信息
                    foreach (['user_name','true_name','password'] as $key => $val) {
                        unset($admin_info[$val]);
                    }
                    Session::set('admin_id', $admin_info['admin_id']);
                    Session::set('admin_info', $admin_info);
                    Session::set('admin_login_expire', getTime()); // 登录有效期
                    session::set('isset_author', "pcfcms"); // 内置勿动
                    adminLog('登陆成功');
                    // 获取当前域名
                    $domain = Request::baseFile();
                    $result = ['code' => 0, 'msg' => '登陆成功','data' => $domain ];
                    return $result;
                } else {
                    adminLog('登陆失败');
                    $result = ['code' => 1, 'msg' => '账号/密码不正确'];
                    return $result;
                }

            }
        }
        $this->assign('web_name', tpCache('web.web_name'));
        $this->assign('is_vertify', $is_vertify);
        $ajaxLogic = new AjaxLogic;
        $ajaxLogic->login_handle();
        return $this->fetch('login');
    }

    // 验证码
    // 作者 pcfcms <1131680521@qq.com>
    public function captcha(){
        $gzpcfglobal = get_global(); // 获取配置参数
        // 验证码插件开关
        $admin_login_captcha = $gzpcfglobal['admin_login'];
        if (!empty($admin_login_captcha['is_on']) && $admin_login_captcha['is_on'] ==1) {
            ob_clean(); // 清空缓存，才能显示验证码
            return Captcha::create('verify');
        }
    }

    //退出登录
    public function logout(){
        adminLog('退出登陆');
        session_unset();
        session::clear();
        Cache::clear('getMenuList');//清空后台栏目导航缓存
        Cache::clear('getmenu');//清空节点缓存
        $result = ['status' => 1, 'msg' => 'ok','url' => url('/login/index')->suffix(true)->domain(true)->build()];
        return $result;
    }
}