<?php
/***********************************************************
 * 短信逻辑定义
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\common\logic;
use lunzi\TpSms;

/*短信类*/
class SmsLogic 
{

    /**
     * 短信发送
     * @param $to    接收人
     */
    public function send_msg($to='',$code){
            // 是否填写短信配置
            $sms_config = tpCache('sms');
            if($sms_config['sms_syn_weapp'] == 1 || !empty($sms_config['sms_syn_weapp'])){
                unset($sms_config['sms_content']);
                unset($sms_config['sms_test']);
                foreach ($sms_config as $key => $val) {
                    if (empty($val)) {
                        return array('code'=>0 , 'msg'=>'该功能待开放，网站管理员尚未完善短信配置！');
                    }
                }
            }else{
                return array('code'=>0 , 'msg'=>'后台短信功能没有开启！');
            }

            $tpSms = new TpSms();
            $sms_appkey = $sms_config['sms_appkey'];
            $sms_secretkey = $sms_config['sms_secretkey'];
            if(isset($sms_config['sms_content']) && !empty($sms_config['sms_content'])){
               $content = $sms_config['sms_content'];//短信内容
            }else{
               $content ="【pcfcms】验证码为：".$code."。尊敬的客户，请在5分钟之内输入该手机动态验证码，过期自动失效。";//短信内容 
            }
            $json = $tpSms->sendsms('1555',$sms_appkey,$sms_secretkey,$to,$content);
            $result1 = json_decode($json, true);
            if($result1['returnstatus'] = "success" && $result1['message'] = "ok"){
                $result = array('code'=>1 , 'msg'=>'发送成功');
            }else{
                $result = array('code'=>0 , 'msg'=>'发送失败！');
            }
            return $result;
    }



}
