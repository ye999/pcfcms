<?php
/***********************************************************
 * 邮箱逻辑定义
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\home\logic;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;
use think\facade\Session;

class SmtpmailLogic
{
    // 发送邮件
    public function send_email($email = '', $title = '', $type = 'reg')
    {
        if (empty($email)) {
            return ['code'=>0, 'msg'=>"邮箱地址参数不能为空！"];
        }
        // 查询扩展是否开启
        $openssl_funcs = get_extension_funcs('openssl');
        if (!$openssl_funcs) {
            return ['code'=>0, 'msg'=>"请联系空间商，开启php的 <font color='red'>openssl</font> 扩展！"];
        }
        // 是否填写邮件配置
        $smtp_config = tpCache('smtp');
		if($smtp_config['smtp_syn_weapp'] == 1 || !empty($smtp_config['smtp_syn_weapp'])){
			unset($smtp_config['smtp_test']);
			foreach ($smtp_config as $key => $val) {
				if (empty($val)) {
					return ['code'=>0, 'msg'=>"该功能待开放，网站管理员尚未完善邮件配置！"];
				}
			}
		}else{
			return ['code'=>0, 'msg'=>"后台邮箱功能没有开启！"];
		}
        $users_id = session::get('pcfcms_users_id');
        if ('retrieve_password' == $type) //找回密码
        {
            // 判断会员是否已绑定邮箱
            $usersdata = Db::name('users')->where('email',$email)->field('email,status')->find();
            if (!empty($usersdata)) {
                if (empty($usersdata['status']) || $usersdata['status'] == 0) {
                    return ['code'=>0, 'msg'=>'该会员尚未激活，不能找回密码！'];
                } else if (empty($usersdata['email'])) {
                    return ['code'=>0, 'msg'=>'邮箱地址未绑定，不能找回密码！'];
                }
                // 数据添加
                $datas['source']    = 4; // 来源，与场景ID对应：0=默认，2=注册，3=绑定邮箱，4=找回密码
                $datas['email']     = $email;
                $datas['code']      = rand(100000,999999);
                $datas['add_time']  = time();
                Db::name('smtp_log')->save($datas);
            }else{
                return ['code'=>0, 'msg'=>'邮箱地址不存在！'];
            }
        }
		else if ('bind_email' == $type) //绑定邮箱
        {
            // 判断邮箱是否已存在
		    $listwhere=[];
			$listwhere[]=['email','=',$email];
			$listwhere[]=['id','<>',$users_id];
            $users_list = Db::name('users')->where($listwhere)->find();
            if(empty($users_list)){
                // 判断会员是否已绑定相同邮箱
				$userswhere=[];
				$userswhere=['id','=',$users_id];
				$userswhere=['email','=',$email];
                $usersdata = Db::name('users')->where($userswhere)->field('email')->find();
                if (!empty($usersdata['email'])) {
                    return ['code'=>0, 'msg'=>'邮箱已绑定，无需重新绑定！'];
                }
                $datas['source']    = 3; // 来源，与场景ID对应：0=默认，2=注册，3=绑定邮箱，4=找回密码
                $datas['email']     = $email;
                $datas['users_id']  = $users_id;
                $datas['code']      = rand(100000,999999);
                $datas['add_time']  = time();
                Db::name('smtp_log')->save($datas);
            }else{
                return ['code'=>0, 'msg'=>"邮箱已经存在，不可以绑定！"];
            }
        }
		else if ('reg' == $type) //注册
        {
            $users_list = Db::name('users')->where('email',$email)->find();
            if (empty($users_list)) {
                $datas['source']    = 2; // 来源，与场景ID对应：0=默认，2=注册，3=绑定邮箱，4=找回密码
                $datas['email']     = $email;
                $datas['code']      = rand(100000,999999);
                $datas['add_time']  = time();
                Db::name('smtp_log')->save($datas);
            }else{
                return ['code'=>0, 'msg'=>'邮箱'.$email.'已被注册'];
            }
        }

        if('admin' == $type) //提醒管理员
		{
			// 判断标题拼接
			$web_name = $title.'-'.tpCache('web.web_name');
			$content = "<p style='text-align:left;'>{$web_name}</p>";
			// 实例化类库，调用发送邮件
			pcfcmssend_email($email, $title, $content, $smtp_config);		
		}
        else{
			// 判断标题拼接
			$web_name = $title.'-'.tpCache('web.web_name');
			$html = '您的邮箱验证码为: '.$datas['code'];
			$content = "<p style='text-align:left;'>{$web_name}</p><p style='text-align:left;'>{$html}</p>";
			// 实例化类库，调用发送邮件
			$res = pcfcmssend_email($email, $title, $content, $smtp_config);
			if (intval($res['code']) == 1) {
				return ['code'=>1, 'msg'=>$res['msg']];
			} else {
				return ['code'=>0, 'msg'=>$res['msg']];
			}			
		}
    }

}
