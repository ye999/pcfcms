<?php
/***********************************************************
 * 搜索页
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\home\controller;
use think\facade\Db;

class Search extends Base
{

    public function initialize() {
        parent::initialize();
    }

    // 搜索主页
    public function index()
    {
        return $this->lists();
    }

    // 搜索列表
    public function lists()
    {
        $param = input('param.');
		
        // 记录搜索词
        $word = $this->request->param('keywords');
        $page = $this->request->param('page');
        if(!empty($word) && 2 > $page){
            $nowTime = time();
            $row = Db::name('search_word')->field('id')->where('word',$word)->find();
            if(empty($row)){
                Db::name('search_word')->insert([
                    'word'      => $word,
                    'sort_order'    => 100,
                    'add_time'  => $nowTime,
                    'update_time'  => $nowTime,
                ]);
            }else{
                Db::name('search_word')->where('id',$row['id'])->update([
                    'searchNum'         =>  Db::raw('searchNum+1'),
                    'update_time'       => $nowTime,
                ]);
            }
        }
        $result = $param;
		
        // 获取当前页面URL
        $result['pageurl'] = '';
        $gzpcf = array('field' => $result);
        $this->gzpcf = array_merge($this->gzpcf, $gzpcf);
        $this->assign('gzpcf', $this->gzpcf);
        $viewfile = 'lists_search';
        return $this->fetch(":{$viewfile}");
    }
}