<?php
/**
 * 单页模型
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\home\model;
use think\facade\Db;
use think\facade\Cache;
class Single
{
    // 初始化
    protected function initialize()
    {
        parent::initialize();
    }

    // 获取单条记录
    public function getInfoByTypeid($typeid)
    {
        $cacheKey = "home_model_Single_getInfoByTypeid_{$typeid}";
        $result = Cache::get($cacheKey);
        if (empty($result)) {
            $field = 'c.*, b.*, a.*, b.aid, a.id as typeid';
            $result = Db::name('arctype')->field($field)
                ->alias('a')
                ->join('archives b', 'b.typeid = a.id', 'LEFT')
                ->join('single_content c', 'c.aid = b.aid', 'LEFT')
                ->where('b.channel', 6)
                ->find($typeid);
            Cache::tag('arctype')->set($cacheKey, $result);
        }
        return $result;
    }
}