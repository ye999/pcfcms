
// 系统升级 js 文件
$(document).ready(function(){
    $("#a_upgrade").click(function(){
        btn_upgrade(this, 0);  
    });
});

function btn_upgrade(obj, type)
{
    var v = '';
    var filelist = $("#upgrade_filelist").html();
    if (undefined == filelist || !filelist) {
        parent.layer.closeAll();
        var alert1 = parent.layer.alert("请清除后台缓存以及Ctrl+F5强制刷新页面，再尝试升级！", {icon: 7, title:false}, function(){
            layer.close(alert1);
            var url = __root_dir__ + "index/clearCache";
            var iframe = $(obj).data('iframe');
            if ('parent' == iframe) {
                workspace.window.location.href = url;
            } else {
                window.location.href = url;
            }
        });
        return false;
    }
    var intro = $("#upgrade_intro").html();
    var notice = $("#upgrade_notice").html();
    intro += '<style type="text/css">.layui-layer-content{height:270px!important}</style>';
    v = notice + intro + '<br/>' + filelist;
    var version = $(obj).data('version');
    var max_version = $(obj).data('max_version');
    var title = '检测系统最新版本：'+version;
    if (0 == type) {
        var btn = ['升级','忽略'];
    } else if (1 == type) {
        var btn = ['升级','忽略','不再提醒'];
    }
    // 显示顶部导航更新提示
    $("#upgrade_filelist", window.parent.document).html($("#upgrade_filelist").html());    
    $("#upgrade_intro", window.parent.document).html($("#upgrade_intro").html());
    $("#upgrade_notice", window.parent.document).html($("#upgrade_notice").html());
    $('#a_upgrade', window.parent.document).attr('data-version',version).attr('data-max_version',max_version).show();
    //询问框
    parent.layer.confirm(v, {
            title: title,
			area: ['580px','400px'],
			btn: btn, //按钮
            btn3: function(index){
                var url = $(obj).data('tips_url');
                $.getJSON(url, {show_popup_upgrade:0}, function(){});
                parent.layer.msg('【核心设置】里可以开启该提醒', {
                    btnAlign: 'c',
                    time: 20000,
                    btn: ['知道了']
                });
                return false;
            }
        }, function(){
            parent.layer.closeAll();
            setTimeout(function(){checkdir(obj);},200);
        }, function(){  
            parent.layer.msg('不升级可能有安全隐患', {
                btnAlign: 'c',
                time: 20000, //20s后自动关闭
                btn: ['明白了']
            });
            return false;
        }
    );   
}

// 检测升级文件的目录权限
function checkdir(obj) 
{
    parent_layer_loading2('检测系统');
    $.ajax({
        type : "POST",
        url  : $(obj).data('check_authority'),
        timeout : 360000,
        data : {filelist:0},
        dataType: 'json',
        success: function(res) {
            parent.layer.closeAll();
            if (1 == res.code) {
                upgrade($(obj));
            } else {
                if (2 == res.data.code) {
                    var alert = parent.layer.alert(res.msg, {icon: 2, title:false});
                } else {
                    var confirm = parent.layer.confirm(res.msg, {
                            title: '检测系统结果',
							area: ['580px','400px'],
							btn: ['关闭']
                        }, function(){
                            parent.layer.close(confirm);
                            return false;
                        }
                    );  
                }
            }
        },
        error: function(request) {
            parent.layer.closeAll();
            parent.layer.alert("检测不通过，可能被服务器防火墙拦截，请添加白名单，或者联系技术协助！", {icon: 2, title:false}, function(){
                top.location.reload();
            });
        }
    }); 
}

// 升级系统
function upgrade(obj)
{
    parent_layer_loading2('升级<font id="upgrade_speed">中</font>');
    var version = $(obj).data('version');
    var max_version = $(obj).data('max_version');
    var timer = '';
    var speed = 0.01;
    $.ajax({
        type : "POST",
        url  :  $(obj).data('upgrade_url'),
        timeout : 360000,
        dataType: 'json',
        beforeSend:function(){
            timer = setInterval(function(){
                random = Math.floor(Math.random()*89+10);
                random = random.toString();
                random = '1.' + random;
                speed = speed + parseFloat(random);
                speed = Math.floor(speed * 100) / 100;
                if (speed >= 98) {
                    speed = 98;
                }
                $('#upgrade_speed', window.parent.document).html(speed+'%');
            }, 500);
        },        
		success: function(res) {
            $('#upgrade_speed', window.parent.document).html('100%');
            clearInterval(timer);
            if(1 == res.code){
                    setTimeout(function(){
                        var finish = false; // 是否升到最新版
                        if (2 == res.code) {
                            var title = res.msg;
                            var btn = ['关闭'];
                        }else if (version < max_version) { 
							// 当前升级之后的版本还不是官方最新版本，将继续连续更新
                            var title = '已升级版本：'+version+'，官方最新版本：'+max_version+'。';
                            var btn = ['开始检测'];
                        } else { 
							// 升级版本是官方最新版本，将引导到备份新数据
                            finish = true;
                            var title = '已升级最新版本！';
                            var btn = ['关闭'];
                            $('#a_upgrade', window.parent.document).hide(); // 隐藏顶部的更新提示
                        }
                        var full = parent.layer.alert(title, {
                                title: false,
                                icon: 1,
                                closeBtn: 0,
                                btn: btn //按钮
                            }, function(){
                                if (version < max_version) { // 当前升级之后的版本还不是官方最新版本，将继续连续更新
                                    top.location.reload();
                                } else { // 升级版本是官方最新版本，将引导到备份新数据
                                    parent.layer.close(full);
                                    var iframe = $(obj).data('iframe');
                                    if ('parent' == iframe) {
                                        top.location.reload();
                                    } else {
                                        top.location.reload();
                                    }
                                }
                            }
                        );
                    },500);
            }else{
                if (res.code && -4 == res.code) {
                    icon = 4;
                } else {
                    icon = 2;
                }
                parent.layer.closeAll();
                parent.layer.alert(res.msg, {icon: icon, title:false}, function(){
                    top.location.reload();
                });
            }
        },
        error: function(request) {
            parent.layer.closeAll();
            parent.layer.alert("升级失败，请第一时间联系技术协助！", {icon: 2, title:false}, function(){
                top.location.reload();
            });
        }
    });                 
}

function parent_layer_loading2(msg)
{
    if (!msg || undefined == msg) {
        var loading = parent.layer.load(3, {
            shade: [0.1] //0.1透明度的白色背景
        });
    } else {
        var loading = parent.layer.msg(
        msg+'...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;请勿刷新页面', 
        {
            icon: 1,
            time: 3600000, //1小时后后自动关闭
            shade: [0.2] //0.1透明度的白色背景
        });
        var index = parent.layer.load(3, {
            shade: [0.1,'#fff'] //0.1透明度的白色背景
        });
    }
    return loading;
}