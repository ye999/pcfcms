<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\facade\Db;
use think\facade\Session;

class TagUser extends Base
{
    // 会员ID
    public $users_id = 0;

    //初始化
    protected function initialize()
    {
        parent::initialize();
        // 会员信息
        $this->users_id = Session::get('users_id');
        $this->users_id = !empty($this->users_id) ? $this->users_id : 0;
    }

    // 会员中心
    public function getUser($type = 'default', $img = '', $currentstyle = '', $txt = '', $txtid = '')
    {
        $result = false;
        $users_open = getUsersConfigData('users.users_open');
        if ('open' == $type) {
            if (empty($users_open) || $users_open == 0) {
                return false;
            }
        }
        if (!empty($users_open)) {
            $url = '';
            $t_uniqid = '';
            switch ($type) {
                case 'login':
                case 'users_center':
                case 'reg':
                case 'logout':
                    $url = url('/user/'.$type);
                    $t_uniqid = md5(time().uniqid(mt_rand(), TRUE));
                    // A标签ID
                    $result['id'] = md5("pcf_{$type}_{$this->users_id}_{$t_uniqid}");
                    // A标签里的文案ID
                    $result['txtid'] = !empty($txtid) ? md5($txtid) : md5("pcf_{$type}_txt_{$this->users_id}_{$t_uniqid}");
                    // 文字文案
                    $result['txt'] = $txt;
                    // 图片文案
                    $result['img'] = $img;
                    // 链接
                    $result['url'] = $url;
                    // 标签类型
                    $result['type'] = $type;
                    // 图片样式类
                    $result['currentstyle'] = $currentstyle;
                    break;
                case 'open':
                    break;
                default:
                    return false;
                    break;
            }
            $result_json = json_encode($result);
            $version = getCmsVersion();
            $hidden = '';
            switch ($type) {
                case 'login':
                case 'reg':
                case 'logout':
                $hidden = <<<EOF
<script type="text/javascript" src="/common/js/tag_user.js"></script>
<script type="text/javascript">
var tag_user_result_json = {$result_json};
tag_user(tag_user_result_json);
</script>
EOF;
                    break;
            }
            $result['hidden'] = $hidden;
        }
        return $result;
    }

}