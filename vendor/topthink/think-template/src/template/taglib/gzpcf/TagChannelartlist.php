<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;

use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;
use app\common\model\Channelfield;

// 获取当前频道的下级栏目的内容列表标签
class TagChannelartlist extends Base
{
    public $tid = '';
    
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->gzpcfglobal = get_global(); // 获取配置参数
        $this->channelfield = new Channelfield();
        $this->tid = input("param.tid/s", ''); // 应用于栏目列表
        // 应用于文档列表
        $aid = input('param.aid/d', 0);
        if ($aid > 0) {
            $cacheKey = 'tagChannelartlist_'.strtolower('home_'.Request::controller().'_'.Request::action());
            $cacheKey .= "_{$aid}";
            $this->tid = cache::get($cacheKey);
            if ($this->tid == false) {
                $this->tid = Db::name('archives')->where('aid', $aid)->value('typeid');
                cache::set($cacheKey, $this->tid);
            }
        }
        // tid为目录名称的情况下
        $this->tid = $this->getTrueTypeid($this->tid);
    }

    /**
     * 获取当前频道的下级栏目的内容列表标签
     * @param string type son表示下一级栏目,self表示当前栏目,top顶级栏目
     * @param boolean $self 包括自己本身
     */
    public function getChannelartlist($typeid = '', $type = 'self')
    {
        $typeid  = !empty($typeid) ? $typeid : $this->tid;
        if (empty($typeid)) {
            $type = 'top'; // 默认顶级栏目
        }
        $result = $this->getSwitchType($typeid, $type);
        return $result;
    }

    /**
     * 获取指定级别的栏目列表
     * @param string type son表示下一级栏目,self表示同级栏目,top顶级栏目
     * @param boolean $self 包括自己本身
     */
    public function getSwitchType($typeid = '', $type = 'son')
    {
        $result = array();
        switch ($type) {
            case 'son': // 下级栏目
                $result = $this->getSon($typeid, false);
                break;
            case 'self': // 同级栏目
                $result = $this->getSelf($typeid);
                break;
            case 'top': // 顶级栏目
                $result = $this->getTop();
                break;
            case 'sonself': // 下级、同级栏目
                $result = $this->getSon($typeid, true);
                break;
        }
        // 处理自定义表字段的值
        if (!empty($result)) {
            // 获取自定义表字段信息
            $map = array('channel_id' => $this->gzpcfglobal['arctype_channel_id'],);
            $fieldInfo = $this->channelfield->getListByWhere($map, '*', 'name');
            $fieldLogic = new \app\home\logic\FieldLogic;
            foreach ($result as $key => $val) {
                if (!empty($val)) {
                    $val = $fieldLogic->handleAddonFieldList($val, $fieldInfo);
                    $result[$key] = $val;
                }
            }
        }
        return $result;
    }

    /**
     * 获取下一级栏目
     * @param string $self true表示没有子栏目时，获取同级栏目
     */
    public function getSon($typeid, $self = false)
    {
        $ChannelType = new \app\common\model\ChannelType;
        $result = array();
        if (empty($typeid)) {
            return $result;
        }
        $map=[];
        if ($self) {
            $map[] = ['id','IN',$typeid];
            $map[] = ['parent_id','IN',$typeid];
        } else {
            $map[] = ['parent_id','IN',$typeid];
        }
        $map[] = ['is_hidden','=',0];
        $map[] = ['status','=',1];
        $map[] = ['is_del','=',0];
        $result = Db::name('arctype')->field('*, id as typeid')
            ->where($map)
            ->order('sort_order asc')
            ->select()->toArray();
        if ($result) {
            $ctl_name_list = $ChannelType->getAll('id,ctl_name', array(), 'id');
            foreach ($result as $key => $val) {
                // 获取指定路由模式下的URL
                if ($val['is_part'] == 1) {
                    $typeurl = $val['typelink'];
                } else {
                    $ctl_name = $ctl_name_list[$val['current_channel']]['ctl_name'];
                    $typeurl = typeurl($ctl_name."/lists", $val);
                }
                $val['typeurl'] = $typeurl;
                // 封面图
                $val['litpic'] = handle_subdir_pic($val['litpic']);
                $result[$key] = $val;
            }
        }
        return $result;
    }

    // 获取当前栏目
    public function getSelf($typeid)
    {
        $ChannelType = new \app\common\model\ChannelType;
        $result = array();
        if (empty($typeid)) {
            return $result;
        }
        $map = [];
        $map[] = ['id','IN',$typeid];
        $map[] = ['is_hidden','=',0];
        $map[] = ['status','=',1];
        $map[] = ['is_del','=',0];
        $result = Db::name('arctype')->field('*, id as typeid')
            ->where($map)
            ->order('sort_order asc')
            ->select()->toArray();
        if ($result) {
            $ctl_name_list = $ChannelType->getAll('id,ctl_name', array(), 'id');
            foreach ($result as $key => $val) {
                // 获取指定路由模式下的URL
                if ($val['is_part'] == 1) {
                    $typeurl = $val['typelink'];
                } else {
                    $ctl_name = $ctl_name_list[$val['current_channel']]['ctl_name'];
                    $typeurl = typeurl($ctl_name."/lists", $val);
                }
                $val['typeurl'] = $typeurl;
                // 封面图
                $val['litpic'] = handle_subdir_pic($val['litpic']);
                $result[$key] = $val;
            }
        }
        return $result;
    }

    // 获取顶级栏目
    public function getTop()
    {
        $ChannelType = new \app\common\model\ChannelType;
        $map = [];
        $map[] = ['parent_id','=',0];
        $map[] = ['is_hidden','=',0];
        $map[] = ['status','=',1];
        $map[] = ['is_del','=',0];
        $result = Db::name('arctype')->field('*, id as typeid')
            ->where($map)
            ->order('sort_order asc')
            ->select()->toArray();
        if ($result) {
            $ctl_name_list = $ChannelType->getAll('id,ctl_name', array(), 'id');
            foreach ($result as $key => $val) {
                // 获取指定路由模式下的URL
                if ($val['is_part'] == 1) {
                    $typeurl = $val['typelink'];
                } else {
                    $ctl_name = $ctl_name_list[$val['current_channel']]['ctl_name'];
                    $typeurl = typeurl($ctl_name."/lists", $val);
                }
                $val['typeurl'] = $typeurl;
                // 封面图
                $val['litpic'] = handle_subdir_pic($val['litpic']);
                $result[$key] = $val;
            }
        }
        return $result;
    }

}